﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string choise = "";
            Notebook one = new Notebook();
            do
            {
                Console.Clear();
                Console.WriteLine("Записная книжка");
                Console.WriteLine("Введите номер элемента меню");
                Console.WriteLine("1 - Добавить новый контакт");
                Console.WriteLine("2 - Редактирование контакта");
                Console.WriteLine("3 - Удалить контакт");
                Console.WriteLine("4 - Просмотреть контакт");
                Console.WriteLine("5 - Вывести список контактов");
                Console.WriteLine("0 - Выход из программы");
                choise = Console.ReadLine();
                switch (choise)
                {
                    case "1":
                        one.CreateNewNote();
                        break;
                    case "2":
                        one.ShowAllNotes();
                        Console.Write("Введите id контакта который необходимо отредактировать: ");
                        one.EditNote(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case "3":
                        one.ShowAllNotes();
                        Console.Write("Введите id контакта который необходимо удалить: ");
                        one.DeleteNote(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case "4":
                        one.ShowAllNotes();
                        Console.Write("Введите id контакта который необходимо просмотреть: ");
                        one.ReadNote(Convert.ToInt32(Console.ReadLine()));
                        Console.ReadKey();
                        break;
                    case "5":
                        one.ShowAllNotes();
                        Console.ReadKey();
                        break;
                    case "0":
                        break;
                    default:
                        Console.WriteLine("Неправильно набран номер");
                        Console.ReadKey();
                        break;
                }
            }
            while (choise != "0");
        }
    }

    public class Notebook
    {
        private List<Note> contacts;

        public List<Note> Contacts
        {
            get { return contacts; }
            set { contacts = value; }
        }

        public Notebook()
        {
            contacts = new List<Note>();
            contacts.Add(new Note("Карамзин", "Николай", "Михайлович", 89213337755, "Россия", new DateTime(1766, 12, 12)));
            contacts.Add(new Note("Лермонтов", "Михаил", "Юрьевич", 89042227755, "Россия", new DateTime(1814, 10, 15)));
            contacts.Add(new Note("Мономах", "Владимир", "Всеволодович", 89114447755, "Киевская Русь", new DateTime(1053, 5, 26)));
            contacts.Add(new Note("Пушкин", "Александр", "Сергеевич", 89521117755, "Россия", new DateTime(1799, 5, 26)));
            contacts.Add(new Note("Тарковский", "Андрей", "Арсеньевич", 89065557755, "СССР", new DateTime(1932, 4, 4)));
        }

        public void CreateNewNote() // Создание новой записи
        {
            Note currentPerson = new Note();
            string message = "";
            Console.Clear();
            Console.Write("Введите имя: ");
            currentPerson.firstName = MandatoryInput();
            Console.Write("Введите фамилию: ");
            currentPerson.lastName = MandatoryInput();
            Console.Write("Введите отчество (не обязательно): ");
            currentPerson.patronimicName = Console.ReadLine();
            Console.Write("Введите номер телефона (только цифры): ");
            currentPerson.phoneNuber = Convert.ToInt64(MandatoryInput());
            Console.Write("Введите страну рождения: ");
            currentPerson.country = MandatoryInput();
            Console.Write("Введите дату рождения (не обязательно): ");
            message = Console.ReadLine();
            if (message!="")
            {
                currentPerson.dateOfBirth = Convert.ToDateTime(message);
            }
            Console.Write("Введите название организации места работы (не обязательно): ");
            currentPerson.company = Console.ReadLine();
            Console.Write("Введите должность (не обязательно): ");
            currentPerson.job = Console.ReadLine();
            Console.Write("Введите прочие заметки (не обязательно): ");
            currentPerson.notes = Console.ReadLine();
            int index = 0;
            while ((index < contacts.Count) && (String.Compare(currentPerson.ToString(), contacts[index].ToString()) > 0))
            {
                index++;
            }
            contacts.Insert(index, currentPerson);
        }

        public void ShowAllNotes() // Просмотр всех созданных записей в общем списке
        {
            int id = 1;
            Console.Clear();
            Console.WriteLine("Список контактов");
            Console.WriteLine("id - Фамилия Имя Номер телефона");
            foreach (Note person in contacts)
            {
                Console.WriteLine(id++ + " - " + person);
            }
        }

        public string MandatoryInput() // Обеспечение заполнения обязательного поля
        {
            string s = "";
            do
            {
                s = Console.ReadLine();
                if (s == "")
                {
                    Console.Write("Это обязательное поле для заполнения. Пожалуйста, заполните его: ");
                }
            }
            while (s == "");
            return s;
        }

        public void ReadNote(int index) // Просмотр ранее созданной записи
        {
            Console.Clear();
            contacts[index-1].DisplayNote();
        }

        public void EditNote(int index) // Редактирование ранее созданной записи
        {
            Note currentPerson = contacts[index - 1];
            string message = "";
            contacts.RemoveAt(index - 1);
            currentPerson.DisplayNote();
            Console.Write("Введите номер поля которое необходимо отредактировать: ");
            switch (Console.ReadLine())
            {
                case "1":
                    Console.Write("Введите новое имя: ");
                    currentPerson.firstName = MandatoryInput();
                    break;
                case "2":
                    Console.Write("Введите новую фамилию: ");
                    currentPerson.lastName = MandatoryInput();
                    break;
                case "3":
                    Console.Write("Введите новое отчество (не обязательно): ");
                    currentPerson.patronimicName = Console.ReadLine();
                    break;
                case "4":
                    Console.Write("Введите новый номер телефона (только цифры): ");
                    currentPerson.phoneNuber = Convert.ToInt64(MandatoryInput());
                    break;
                case "5":
                    Console.Write("Введите новую страну рождения: ");
                    currentPerson.country = MandatoryInput();
                    break;
                case "6":
                    Console.Write("Введите новую дату рождения (не обязательно): ");
                    message = Console.ReadLine();
                    if (message != "")
                    {
                        currentPerson.dateOfBirth = Convert.ToDateTime(message);
                    }
                    break;
                case "7":
                    Console.Write("Введите новое название организации места работы (не обязательно): ");
                    currentPerson.company = Console.ReadLine();
                    break;
                case "8":
                    Console.Write("Введите новую должность (не обязательно): ");
                    currentPerson.job = Console.ReadLine();
                    break;
                case "9":
                    Console.Write("Введите новые прочие заметки (не обязательно): ");
                    currentPerson.notes = Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Неправильно набран номер");
                    break;
            }
            int secondIndex = 0;
            while ((secondIndex < contacts.Count) && (String.Compare(currentPerson.ToString(), contacts[secondIndex].ToString()) > 0))
            {
                secondIndex++;
            }
            contacts.Insert(secondIndex, currentPerson);
        }

        public void DeleteNote(int index) // Удаление ранее созданной записи
        {
            contacts.RemoveAt(index - 1);
        }
    }

    public class Note
    {
        public string firstName;
        public string lastName;
        public string patronimicName;
        public long phoneNuber;
        public string country;
        public DateTime dateOfBirth;
        public string company;
        public string job;
        public string notes;

        public override string ToString()
        {
            return this.lastName + " "+ this.firstName + " " + this.phoneNuber;
        }

        public void DisplayNote() // Просмотр созданной записи
        {
            Console.WriteLine("1 - Имя: " + this.firstName);
            Console.WriteLine("2 - Фамилия: " + this.lastName);
            Console.WriteLine("3 - Отчество: " + this.patronimicName);
            Console.WriteLine("4 - Телефон: " + this.phoneNuber);
            Console.WriteLine("5 - Страна: " + this.country);
            Console.WriteLine("6 - Дата рождения: " + this.dateOfBirth);
            Console.WriteLine("7 - Организация: " + this.company);
            Console.WriteLine("8 - Должность: " + this.job);
            Console.WriteLine("9 - Прочие заметки: " + this.notes);
        }

        public Note()
        {     
        }

        public Note(string last, string first, string patronimic, long phone, string c, DateTime date)
        {
            firstName = first;
            lastName = last;
            patronimicName = patronimic;
            phoneNuber = phone;
            country = c;
            dateOfBirth = date;
        }
    }
}
